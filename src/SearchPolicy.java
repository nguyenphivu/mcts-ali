import java.util.LinkedList;

/* Super class to search policies (algorithms)
 * Each policy has: number of units of budget used (numCalls),
 * a maximum budget (budget), a root node and sequence of operations.
 */
public class SearchPolicy{

	public int numCalls;
	public int budget;
	public Node root;
	public LinkedList<Operation> sequence;

	public SearchPolicy(){
		setBudget(0);
		setNumCalls(0);
		this.sequence = new LinkedList<Operation>();
	}
	
	public void setNumCalls(int numCalls){
		this.numCalls = numCalls;
	}
	
	public void setBudget(int budget){
		this.budget = budget;
	}
	
	public void setRoot(Node root){
		this.root = root;
	}
	
	public int getNumCalls(){
		return this.numCalls;
	}
	
	public int getBudget(){
		return this.budget;
	}
	
	public Node getRoot(){
		return this.root;
	}

	public void stop() {
		
	}
}
