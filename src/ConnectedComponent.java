
public class ConnectedComponent {
	public int noPixels; //# pixels in this connected component
	public int top; //the top boundary of this connected component
	public int bottom; //the bottom boundary of this connected component
	public int left; //the left boundary of this connected component
	public int right; //the right boundary of this connected component
	public int label; 
	
	public ConnectedComponent(int x, int y){
		this.top= y;
		this.bottom= y;
		this.left= x;
		this.right= x;
	}
	
	public ConnectedComponent(){
		
	}
}
