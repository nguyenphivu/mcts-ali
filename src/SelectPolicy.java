/* UCB-1 select policy
 * If an operation has never been explored, it is selected automatically
 * Otherwise, the operation selected is the one returned with the UCB-1 policy
 */
abstract class SelectPolicy{

	public OperationsBank operationsBank;

	public void setOperationsBank(OperationsBank operationsBank2){
		this.operationsBank = operationsBank2;
	}

	public OperationsBank getOperationBank(){
		return this.operationsBank;
	}

	abstract Operation apply(Node state);
}
