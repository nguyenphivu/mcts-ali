import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

public class Test {
	public static void main (String[] arg){

		Reward reward = null;
		Node root = null;
		SimPolicy simPolicy = null;
		SelectPolicy selectPolicy = null;
		OperationsBank operationsBank = null;
		ImgProcSearchPolicyMcts mcts = new ImgProcSearchPolicyMcts();
		Path bestPath = new Path();/*THIS bestPath IS SHARED AMONG ALL THE SEARCH COMPONENTS AND Parameter class*/
		LinkedList<Operation> bestBranch = new LinkedList<Operation>();

		// Load the OPENCV native library.
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		//Initialize all the objects needed for running the project
		ImgProcReward imgProcReward = new ImgProcReward();
		ImgProcNode imgProcRoot = new ImgProcNode();
		ImgProcSimPolicy imgProcSimPolicy = new ImgProcSimPolicy();
		ImgProcOperationsBank imgProcOperationsBank = new ImgProcOperationsBank();
		ImgProcParameters param = new ImgProcParameters();
		ImgProcSelectPolicy imgProcSelectPolicy = new ImgProcSelectPolicy();

		/********** Set parameters **********/
		param.setParamFile("/media/6E84DB9D84DB65DD/NPV-Data/DNA-Band-Size-Estimation/Java-Projects/MCTS-Ali/parameters.txt"/*the address of the parameter file*/);
		param.setDepth(param.getInt("depth", '=')); //read the depth value from the parameter file then store its value in the parameter class
		param.setBudget(param.getInt("budget", '='));
		param.setNbRepetitions1(param.getInt("nbRepetition1", '='));
		param.setNbRepetitions2(param.getInt("nbRepetition2", '='));
		param.setHyperParam(param.getDouble("hyperParameter", '='));
		param.setSelectPolicy(param.getString("selectPolicy", '='));
		param.setSimPolicy(param.getString("simPolicy", '='));
		param.setOriginalAddress(param.getString("originalImgAddress", '='));
		param.setLabeledImgAddress(param.getString("labeledImgAddress", '='));
		param.setTestOriginalAddress(param.getString("testOriginalImgAddress", '='));
		param.setTestLabeledImgAddress(param.getString("testLabeledImgAddress", '='));
		param.setReportAdr(param.getString("reportAddress", '='));
		param.setBestPath(bestPath);/*THIS bestPath IS SHARED AMONG ALL THE SEARCH COMPONENTS AND Parameter class*/
		param.setOs(param.getString("os", '='));
		param.setRam(param.getDouble("memory", '='));
		param.setProcessor(param.getString("processor", '='));
		param.setJavaVersion(param.getDouble("javaVersion", '='));
		param.setSeed(param.getInt("seed", '='));
		param.setMctsTemplate(param.getString("mctsTemplate", '='));
		param.setLearningImgSize(param.getInt("topX", '='), param.getInt("topY", '='), param.getInt("width", '='), param.getInt("height", '='));
		param.setCrop(param.getBoolean("crop", '='));
		param.setDownsample(param.getBoolean("downsample", '='));
		param.setDownsamplingRate(param.getDouble("downsamplingRate", '='));
		param.setAlpha(param.getDouble("alpha", '='));
		param.setRadius(param.getDouble("radius", '='));
		param.setMinArea(param.getDouble("minArea", '='));

		/*** Add filters features (just for writing the report) ***/
		
		param.addFilter(1, "Gabor", 28, 10, -1);
		param.addFilter(2, "Gabor", 28, 20, -1);
		param.addFilter(3, "Gabor", 28, 30, -1);
		param.addFilter(4, "Gabor", 28, 40, -1);
		param.addFilter(5, "Gabor", 28, 50, -1);
		param.addFilter(6, "Gabor", 28, 60, -1);
		param.addFilter(7, "Gabor", 28, 70, -1);
		param.addFilter(8, "Gabor", 28, 80, -1);
		param.addFilter(9, "Gabor", 28, 90, -1);
		param.addFilter(10, "Gabor", 28, 100, -1);

		param.addFilter(11, "Thresholding", -1, -1, -1);
		param.addFilter(12, "Thresholding", -1, -1, -1);
		param.addFilter(13, "Thresholding", -1, -1, -1);
		param.addFilter(14, "Thresholding", -1, -1, -1);
		param.addFilter(15, "Thresholding", -1, -1, -1);

		param.addFilter(16, "Gaussian", -1, 4, -1);
		param.addFilter(17, "Gaussian", -1, 6, -1);
		param.addFilter(18, "Gaussian", -1, 8, -1);
		param.addFilter(19, "Gaussian", -1, 16, -1);

		param.addFilter(20, "Opening", -1, -1, 1);
		param.addFilter(21, "Opening", -1, -1, 2);

		param.addFilter(22, "Closing", -1, -1, 1);
		param.addFilter(23, "Closing", -1, -1, 2);

		param.addFilter(24, "Stop", -1, -1, -1);

		/*** OPERATIONS BANK BUILDING ***/
		
		//Need to modify here when adding new filters (extending FiltersBank)
		for(int i = 1; i < 25/*total # filters in the FiltersBank + stop filter*/; i++){

			ImgProcOperation op = new ImgProcOperation();
			op.num = i;//make sure that the operation filter number here is the same as in the FiltersBank, and this is the
			//the ONLY link between operation and FilterBank

			if(i < 11){
				op.name = "Gabor";
				//imgProcOperationsBank.addOperation(i, "Gabor");
			}else if(i < 16){
				op.name = "Thresholding";
			}else if(i < 20){
				op.name = "Gaussian";
			}else if(i < 22){
				op.name = "Opening";
			}else if(i < 24){
				op.name = "Closing";
			}else{
				op.name = "Stop";
			}

			imgProcOperationsBank.addOperation((Operation)op);
		}
		
		//imgProcOperationsBank.display();

		/*** Init best path ***/
		//
		bestPath.setBestReward(-1);
		bestPath.setBestBranch(bestBranch);

		/*** Init root ***/
		
		imgProcRoot.level = 0;
		imgProcRoot.setOperationsBank(imgProcOperationsBank);
		imgProcRoot.depthMax = param.depth;
		
		/*** Init simulate policy ***/
		
		imgProcSimPolicy.setOperationsBank(imgProcOperationsBank);
		imgProcSimPolicy.setseed(param.seed);//the seed is only needed in the simulate policy 
		
		/*** Init select policy ***/

		imgProcSelectPolicy.setOperationsBank(operationsBank);
		imgProcSelectPolicy.setHyperParameter(param.hyperParam);//hyperParam is for UCB1
		
		/** Init reward function**/
		imgProcReward.setParam(param);
		//imgProcReward.setAlpha(param.alpha);

		/*** Set learning and test images ***/
		
		Mat img = new Mat();
		Rect rectCrop = new Rect(param.topX, param.topY, param.width, param.height);//set the cropping region for all learning images 
		Size size = new Size();
		String name = "";

		/*** Learning set ***/
		
		File repo1/*repository1*/ = new File(param.originalImgAddress);//the folder containing all the original images
		String[] imgSet1 = repo1.list();//get all the file names inside the folder
		File repo2 = new File(param.labeledImgAddress);//the folder containing all the labeled images
		String[] imgSet2 = repo2.list();

		//do pre-processing (crop or downsample, resize, etc.) on all original images
		for(int i = 0; i < imgSet1.length; i++){
			
			int index = 0;
			img  = Highgui.imread(param.originalImgAddress+"/"+imgSet1[i]);
			Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2GRAY);
			name = imgSet1[i].substring(0, imgSet1[i].indexOf('.'));
			if(param.imgNames.size() == 0){
				index = 0;
			}else{
				index = 0;
				while((index < param.imgNames.size()) && (Integer.parseInt(param.imgNames.get(index)) < Integer.parseInt(name))){
					index++;
				}
			}
			param.imgNames.add(index, name);
			param.originalImg.add(index, img);
			if(param.crop){
				img = img.submat(rectCrop);
			}else if (param.downsample){
				size.height = (int)(img.height()*param.downsamplingRate);
				size.width = (int) (img.width()*param.downsamplingRate);
				Imgproc.resize(img, img, size, param.downsamplingRate, param.downsamplingRate, Imgproc.INTER_LINEAR);
			}
			System.out.println(img.size());
			imgProcReward.input.add(index, img);
			
			//look for the corresponding labeled image,
			//crop or downsample accordingly 
			for(int j = 0; j < imgSet2.length; j++){
				if(imgSet2[j].contains(name)){
					img  = Highgui.imread(param.labeledImgAddress+"/"+imgSet2[j]);
					Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2GRAY);
					param.labeledImg.add(index, img);
					if(param.crop){
						img = img.submat(rectCrop);
					}else if (param.downsample){
						size.height = (int)(img.height()*param.downsamplingRate);
						size.width = (int) (img.width()*param.downsamplingRate);
						Imgproc.resize(img, img, size, param.downsamplingRate, param.downsamplingRate, Imgproc.INTER_LINEAR);
					}
					imgProcReward.labeledImages.add(index, img);
					break;
				}
			}
		}
		
		/** Find connected components for all the labelled images **/
		for (int i=0;i<imgProcReward.labeledImages.size();i++)
			ImgProcReward.labeledConCompLists.add(new ArrayList<ConnectedComponent>());
		for (int i=0;i<imgProcReward.labeledImages.size();i++){
			ImgProcReward.findConnectedComponents(imgProcReward.labeledImages.get(i), new Mat(), ImgProcReward.labeledConCompLists.get(i));
		}
		
		/*** Test set ***/
		//Do the same things as in the learning set
		
		File testRepo1 = new File(param.testOriginalImgAddress);
		String[] testImgSet = testRepo1.list();
		File testRepo2 = new File(param.testLabeledImgAddress);
		String[] testImgSet2 = testRepo2.list();
		
		for(int i = 0; i < testImgSet.length; i++){

			int index = 0;
			img  = Highgui.imread(param.testOriginalImgAddress+"/"+testImgSet[i]);
			Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2GRAY);
			name = testImgSet[i].substring(0, testImgSet[i].indexOf('.'));
			if(param.imgNames.size() == 0){
				index = 0;
			}else{
				index = 0;
				while((index < param.testImgNames.size()) && (Integer.parseInt(param.testImgNames.get(index)) < Integer.parseInt(name))){
					index++;
				}
			}

			param.testOriginalImg.add(index, img);
			param.testImgNames.add(index, name);
			
			for(int j = 0; j < testImgSet2.length; j++){
				if(testImgSet2[j].contains(name)){
					img  = Highgui.imread(param.testLabeledImgAddress+"/"+testImgSet2[j]);
					Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2GRAY);
					param.testLabeledImg.add(index, img);
					break;
				}
			}
		}
		
		//not necessary
		simPolicy = imgProcSimPolicy;
		selectPolicy = imgProcSelectPolicy;
		operationsBank = imgProcOperationsBank;
		root = imgProcRoot;
		reward = imgProcReward;
		
		/*** Init mcts algorithm ***/

		mcts.setBestPath(bestPath);/*THIS bestPath IS SHARED AMONG ALL THE SEARCH COMPONENTS AND Parameter class*/
		mcts.setBudget(param.budget);
		mcts.setOperationsBank(operationsBank);
		mcts.setReward(reward);
		mcts.setRoot(root);
		mcts.setDepth(param.depth);

		/*** Apply mcts algorithm***/
		
		param.getStartTime();
		try{
			param.createReportFolder();
			param.setUpFractionGraphDataFile(param.reportFolderAddress);
			imgProcReward.setUpGraphDataFile(param.reportFolderAddress);
			
			//THE MAIN FUNCTION OF MCTS
			mcts.apply(selectPolicy, simPolicy/*simulation policy*/, param);
			//to let the report-writing know if the algorithm run finishes
			param.setComputationCompleted();
			imgProcReward.finishGraphDataWriting();
			param.finishFractionGraphDataWriting();
			
		}catch(Exception e){
			//Any exceptions are put into the report
			param.getEndTime();
			param.setRunTime();
			System.out.println((char)27 + "[31mComputation not completed!");
			e.printStackTrace();
			imgProcReward.finishGraphDataWriting();
			param.finishFractionGraphDataWriting();
			param.editLateXReport();
		}		

		if(param.computationCompleted){
			System.out.println((char)27 + "[32mComputation completed");
		}else{
			if(mcts.numCalls == mcts.budget){
				System.out.println((char)27 + "[31mOut of budget!");
			}	
		}
		param.getEndTime();
		param.setRunTime();//this value - param.getStartTime() = TOTAL RUNNING TIME
		param.editLateXReport();
		
		for(int i = 0; i < param.visitedNodesFraction.size(); i++){
			System.out.println(param.visitedNodesFraction.get(i));
		}
	}
}
