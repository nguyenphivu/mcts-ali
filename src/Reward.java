import java.util.LinkedList;

abstract class Reward {

	public LinkedList<Operation> output;

	abstract LinkedList<LinkedList<Double>> calculate(LinkedList<Operation> sequence, Node root);

}
