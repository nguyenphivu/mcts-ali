
public class ImgProcSelectPolicy extends SelectPolicy{


	public double hyperParameter;
	
	public ImgProcSelectPolicy(){
		setOperationsBank(null);
		setHyperParameter(0);
	}

	public void setHyperParameter(double c){
		this.hyperParameter = c;
	}

	public double getHyperParameter(){
		return this.hyperParameter;
	}
	
	public Operation apply(Node state){

		int l = state.childrenList.size();
		int bestOperation = 0;
		double arg;
		double argmax = 0.0;
		double s;
		int n,N;
		Operation op;

		for (int i = 0; i < l; i++ ){
			//using formula (6) in [Maes et al., 2013]
			op = state.childrenList.get(i).prevOperation;
			s = state.rewardActionSelect[op.num - 1]; //s(x,u)
			n = state.nbActionSelect[op.num - 1]; //n(x,u)
			N = state.nbStateSelect; //n(x)

			//EXPLORATION
			if (n == 0){
				return state.childrenList.get(i).prevOperation;
			}
			//EXPLOITATION
			else{
				arg = Math.log(N) / n;
				arg = Math.sqrt(arg);		
				arg = arg * this.hyperParameter;
				arg = arg + (s/n);

				if(arg > argmax){
					argmax = arg;
					bestOperation = i;
				}
			}
		}
		
		return state.childrenList.get(bestOperation).prevOperation;
	}
}
