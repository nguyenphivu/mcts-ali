
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.imgproc.*;
import org.opencv.core.*;

import java.io.*;
import java.math.*;
import java.text.DecimalFormat;
import java.util.*;

//Each filter has a id number:
//1-10: Gabor filters
//11-15: Otsu Thresholding filters
//16-19: Gaussian filters
//20-21: opening filters
//22-23: closing filters
//24: stop filter

//Each filter of a same type has a second-level id number to differentiate among different sets of parameters
//PLEASE LOOK AT THE MAIN FUNCTION FOR AN EXAMPLE OF HOW TO USE THE FILTER BANK

@SuppressWarnings("unused")
public class FiltersBank {

	public FiltersBank(){
		
	}
	
	public static void displayImage(Mat image, String title){
		
		Mat displayedImg= new Mat();
		Imgproc.resize(image, displayedImg, new Size(1000,(int)(image.height()*1000.0)/image.width()));
	    MatOfByte matOfByte = new MatOfByte();
	    Highgui.imencode(".jpg", displayedImg, matOfByte);
	    try {
	        JFrame frame = new JFrame(title);
	        frame.getContentPane().add(new JLabel(new ImageIcon(ImageIO.read(new ByteArrayInputStream(matOfByte.toArray())))));
	        frame.pack(); //Causes this Window to be sized to fit the preferred size and layouts of its subcomponents
	        frame.setVisible(true);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
	public static int gaborFilter(int filterNumber, Mat imageIn, Mat imageOut){
		int kernelSize= 24;
		double sigma=0, lambda=0, gamma=1, psi=1.5, theta=Math.PI/2;
		switch (filterNumber){
			case 1: {
				//Set parameters for Gabor filter
				lambda= 28;
				sigma= 10;
				break;
			}
			case 2: {
				lambda= 28;
				sigma= 20;
				break;
			}
			case 3: {
				lambda= 28;
				sigma= 30;
				break;
			}
			case 4: {
				lambda= 28;
				sigma= 40;
				break;
			}
			case 5: {
				lambda= 28;
				sigma= 50;
				break;
			}
			case 6: {
				lambda= 28;
				sigma= 60;
				break;
			}
			case 7: {
				lambda= 28;
				sigma= 70;
				break;
			}
			case 8: {
				lambda= 28;
				sigma= 80;
				break;
			}
			case 9: {
				lambda= 28;
				sigma= 90;
				break;
			}
			case 10: {
				lambda= 28;
				sigma= 100;
				break;
			}
		}
		
		//Do Gabor filter with the parameters as set above
		Mat gaborKernel= Imgproc.getGaborKernel(new Size(kernelSize, kernelSize), sigma, theta/*horizontal*/, 
				lambda, gamma, psi, CvType.CV_32F);
		Imgproc.filter2D(imageIn, imageOut, -1, gaborKernel);
		
		return 1;//successful
	}
	
	public static int gaussianFilter(int filterNumber, Mat imageIn, Mat imageOut){
		double sigma= 1;
		switch (filterNumber){
			case 1: {
				//Set parameter for the Gaussian filter
				sigma= 4;
				break;
			}
			case 2: {
				sigma= 6;
				break;
			}
			case 3: {
				sigma= 8;
				break;
			}
			case 4: {
				sigma= 16;
				break;
			}
		}
		
		//Do Gaussian filter with the parameters as set above
		Imgproc.GaussianBlur(imageIn, imageOut, new Size(21, 21), sigma);
		
		return 1;//successful
	}
	
	public static int dilationFilter(int filterNumber, Mat imageIn, Mat imageOut){
		//the input image should be in gray scale
		if (imageIn.channels()!=1)
			return 0;
		int dilationSize= 1; 
		switch (filterNumber){
			case 1: {
				//Set parameter for dilation
				dilationSize= 1;
				break;
			}
			case 2: {
				dilationSize= 2;
				break;
			}
		}
	
		//Do dilation with the parameters as set above
		Mat element= Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size( 2*dilationSize + 1, 2*dilationSize+1 ),
               new Point( dilationSize, dilationSize ));
		Imgproc.dilate(imageIn, imageOut, element);
		
		return 1;//successful
	}
	
	public static int erosionFilter(int filterNumber, Mat imageIn, Mat imageOut){
		//the input image should be in gray scale
		if (imageIn.channels()!=1)
			return 0;
		int erosionSize= 1; 
		switch (filterNumber){
			case 1: {
				//Set parameter for dilation
				erosionSize= 1;
				break;
			}
			case 2: {
				erosionSize= 2;
				break;
			}
		}
		
		//Do erosion with the parameters as set above
		Mat element= Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size( 2*erosionSize + 1, 2*erosionSize+1 ),
	               new Point(erosionSize, erosionSize));
		Imgproc.erode(imageIn, imageOut, element);
		
		return 1;//successful
	}
	
	//It's better to use dilation and erosion together
	public static int openingFilter(int filterNumber, Mat imageIn, Mat imageOut){
		//the input image should be in gray scale
		if (imageIn.channels()!=1)
			return 0;
		int openingSize= 1; 
		switch (filterNumber){
			case 1: {
				//Set parameter for dilation
				openingSize= 1;
				break;
			}
			case 2: {
				openingSize= 2;
				break;
			}
		}
		
		FiltersBank.erosionFilter(openingSize, imageIn, imageOut);
		FiltersBank.dilationFilter(openingSize, imageIn, imageOut);
		
		return 1;//successful
	}
	
	//It's better to use dilation and erosion together
	public static int closingFilter(int filterNumber, Mat imageIn, Mat imageOut){
		//the input image should be in gray scale
		if (imageIn.channels()!=1)
			return 0;
		int openingSize= 1; 
		switch (filterNumber){
			case 1: {
				//Set parameter for dilation
				openingSize= 1;
				break;
			}
			case 2: {
				openingSize= 2;
				break;
			}
		}
		
		FiltersBank.dilationFilter(openingSize, imageIn, imageOut);
		FiltersBank.erosionFilter(openingSize, imageIn, imageOut);
		
		return 1;//successful
	}
	
	public static int thresholdingFilter(Mat imageIn, Mat imageOut){
		//the input image should be in gray scale
		if (imageIn.channels()!=1)
			return 0;
	
		//Do thresholding with the parameters as set above
		Imgproc.threshold(imageIn, imageOut, 0, 255, Imgproc.THRESH_BINARY|Imgproc.THRESH_OTSU);
		
		return 1;//successful
	}
	
	public static int stopFilter(Mat imageIn, Mat imageOut){
		imageIn.copyTo(imageOut);
		return 1;
	}
	
	//Filter number:
	//1-10: Gabor filters
	//11-20: Thresholding filters
	//21-24: Gaussian filters
	//25-26: dilation filters
	//27-28: erosion filters
	//29: stop filter
	public static int filter(int filterNumber, Mat imageIn, Mat imageOut){
		int success= 0;
		switch (filterNumber){
		//Gabor filters	
		case 1: {
				success= FiltersBank.gaborFilter(1, imageIn, imageOut);
				break;
			}
			case 2: {
				success= FiltersBank.gaborFilter(2, imageIn, imageOut);
				break;
			}
			case 3: {
				success= FiltersBank.gaborFilter(3, imageIn, imageOut);
				break;
			}
			case 4: {
				success= FiltersBank.gaborFilter(4, imageIn, imageOut);
				break;
			}
			case 5: {
				success= FiltersBank.gaborFilter(5, imageIn, imageOut);
				break;
			}
			case 6: {
				success= FiltersBank.gaborFilter(6, imageIn, imageOut);
				break;
			}
			case 7: {
				success= FiltersBank.gaborFilter(7, imageIn, imageOut);
				break;
			}
			case 8: {
				success= FiltersBank.gaborFilter(8, imageIn, imageOut);
				break;
			}
			case 9: {
				success= FiltersBank.gaborFilter(9, imageIn, imageOut);
				break;
			}
			case 10: {
				success= FiltersBank.gaborFilter(10, imageIn, imageOut);
				break;
			}
			//Thresholding filters
			case 11: {
				success= FiltersBank.thresholdingFilter(imageIn, imageOut);
				break;
			}
			case 12: {
				success= FiltersBank.thresholdingFilter(imageIn, imageOut);
				break;
			}
			case 13: {
				success= FiltersBank.thresholdingFilter(imageIn, imageOut);
				break;
			}
			case 14: {
				success= FiltersBank.thresholdingFilter(imageIn, imageOut);
				break;
			}
			case 15: {
				success= FiltersBank.thresholdingFilter(imageIn, imageOut);
				break;
			}
			//Gaussian filters
			case 16: {
				success= FiltersBank.gaussianFilter(1, imageIn, imageOut);
				break;
			}
			case 17: {
				success= FiltersBank.gaussianFilter(2, imageIn, imageOut);
				break;
			}
			case 18: {
				success= FiltersBank.gaussianFilter(3, imageIn, imageOut);
				break;
			}
			case 19: {
				success= FiltersBank.gaussianFilter(4, imageIn, imageOut);
				break;
			}
			//dilation filters
			/*
			case 25: { 
				success= FiltersBank.dilationFilter(1, imageIn, imageOut);
				break;
			}
			case 26: { 
				success= FiltersBank.dilationFilter(2, imageIn, imageOut);
				break;
			}
			*/
			//erosion filters
			/*
			case 27: { 
				success= FiltersBank.erosionFilter(1, imageIn, imageOut);
				break;
			}
			case 28: { 
				success= FiltersBank.erosionFilter(2, imageIn, imageOut);
				break;
			}
			*/
			//opening filters
			case 20: { 
				success= FiltersBank.openingFilter(1, imageIn, imageOut);
				break;
			}
			case 21: { 
				success= FiltersBank.openingFilter(2, imageIn, imageOut);
				break;
			}
			//closing filters
			case 22: { 
				success= FiltersBank.closingFilter(1, imageIn, imageOut);
				break;
			}
			case 23: { 
				success= FiltersBank.closingFilter(2, imageIn, imageOut);
				break;
			}
			//stop filter
			case 24: { 
				success= FiltersBank.stopFilter(imageIn, imageOut);
				break;
			}
		}
		
		return success;
	}
	
	public static int runFilters(LinkedList<Integer> filterSeq, Mat imageIn, Mat imageOut){
		Mat intermediateImg= new Mat();
		int success= 0;
		for (int i=0;i<filterSeq.size();i++){
			success= FiltersBank.filter(filterSeq.get(i), imageIn, intermediateImg);
			if (success==0)
				return 0;
			intermediateImg.copyTo(imageIn);
		}
		intermediateImg.copyTo(imageOut);
		return 1;
	}
}
