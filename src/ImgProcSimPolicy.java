import java.util.Random;

/* Defines the simulation policy to use
 * A random simulator is used and it is associated
 * to a seeded random number generator.
 * A random number is generated, ifit is inferior to the 
 * children vector size it returns the corresponding child.
 * Otherwise, an other number is generated.
 */
public class ImgProcSimPolicy extends SimPolicy{

	public ImgProcOperationsBank operationsBank;
	static Random rndNumbers;
	static int randomVal;


	public ImgProcSimPolicy(){
		operationsBank = new ImgProcOperationsBank();
		ImgProcSimPolicy.rndNumbers = new Random();
		ImgProcSimPolicy.randomVal = 0;
	}

	public void setOperationsBank(ImgProcOperationsBank operationsBank2) {
		this.operationsBank = operationsBank2;
	}

	public void setseed(int seed){
		ImgProcSimPolicy.rndNumbers.setSeed(seed);
	}

	public Operation apply(Node node) {

		ImgProcNode newNode = new ImgProcNode();
		newNode = (ImgProcNode)node;

		//if the selection reaches the leaf node of the selection tree
		if(newNode.hasChildren() == false){
			//add/expand all the possible children to newNode
			newNode.expand();
		}

		/*----GET ANY OPERATION THAT CAN CREATE A CHILD OF newNode ---*/
		// generate a random index number from 0 to newNode's #children
		ImgProcSimPolicy.randomVal = ImgProcSimPolicy.rndNumbers.nextInt(newNode.childrenList.size());
		ImgProcOperation p = new ImgProcOperation();
		p = (ImgProcOperation) newNode.childrenList.get(ImgProcSimPolicy.randomVal).prevOperation;
		
		return (Operation)p;
	}
}