import java.util.LinkedList;

/* For each remaining time step, it runs the sub-search component,
 * extracts the next operation from the best sequence obtained so far
 * and performs the transition to the next state. 
 */
public class Step extends SearchComponent{

	public SearchComponent searchComp;

	public Step(){

		super();
		setSearchComponent(null);
		this.name = "Step";
	}

	public void setSearchComponent(SearchComponent serachComp){
		this.searchComp = serachComp;
	}

	public SearchComponent getSearchComponent(){
		return this.searchComp;
	}

	@SuppressWarnings("unchecked")
	//Function's parameters are as in [Maes et al., 2013]
	public void apply(LinkedList<Operation> sequence /*(u1,u2,..,u(t-1))*/, Node state /*x(t)*/){

		//System.out.println("Step!");

		int t = state.level;//level of the root node is 0
		Node nextState;
		Node copy;
		Operation op;
		LinkedList<Operation> nextSeq = new LinkedList<Operation>();
		nextState = state.copy();
		nextSeq = (LinkedList<Operation>) sequence.clone();
		int nbNodesVisitedKept = 0;
		int nbNodesVisitedDiscarded = 0;
		double fraction = 0.0;
		Node child;
		
		//Debug purpose:
		if(t != sequence.size()){
			System.out.println("Step problem (something wrong happens)");
		}

		for(int i = t; i < maxDepth /*maxDepth T*/; i++){

			System.out.println("Step to level " + i);
			this.param.setStep(i);

			super.invoke(this.searchComp, nextSeq, nextState); // sub search 

			//update the best path from root to depth i
			//nextSeq.clear();
			//for(int j = 0; j < i+1; j++){
			//	nextSeq.add(bestPath.bestBranch/*bestPath: the current best path and all related info e.g. best reward*/.get(j));
			//}

			//update the best path from root to depth i
			op = bestPath.bestBranch.get(i); // extracts next (current best) operation 
			nextSeq.add(op);
			
			if(op.name.equals("Stop")){
				break;
			}
			
			// transition to next state 
			copy = nextState.OperationFindChild(op);
			
			/*--FOR CALCULATING THE FRACTION GRAPH (could be implemented in ImgProcParameters as a method)--*/
			for(int k = 0; k < nextState.childrenList.size(); k++){
				child = nextState.childrenList.get(k);
				if(child.equals(copy)){
					nbNodesVisitedKept = child.getNbVisitedChildren();
				}else{
					if(child.nbStateSelect > 0)/*if child is not a leaf node in the current selection tree*/{
						nbNodesVisitedDiscarded = nbNodesVisitedDiscarded + child.getNbVisitedChildren();
					}
				}
			}
			//
			fraction = (double)nbNodesVisitedKept / (double)nbNodesVisitedDiscarded;
			this.param.addVisitedNodesFraction(fraction);
			this.param.fractionOutputWriter.println((i+1)+"" + "\t" + fraction+"");
			nbNodesVisitedDiscarded = 0;
			nbNodesVisitedKept = 0;
			/*----------------------------------*/
			
			if(nextState.hasChildren()){
				nextState.childrenList.clear();
			}
			nextState.childrenEdges.clear();
			nextState = copy;
		}//end for i
	}
}	