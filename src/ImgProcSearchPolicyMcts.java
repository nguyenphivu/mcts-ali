/* Implements a uct algorithm, Monte Carlo with a random simulator
 * and UCB-1 select policy
 * uct = mcts(ucb-1, randomsim, N1, N2)
 * 	   = Step(Repeat(N2, Select(Lookahead(Repeat(N1, Simulate)))))	
 */
public class ImgProcSearchPolicyMcts extends SearchPolicy{
	
	public OperationsBank operationsBank;
	public Reward reward;
	public Path bestPath;
	public int maxDepth;
	
	public ImgProcSearchPolicyMcts(){
		super();
		bestPath = new Path();
		maxDepth = 0;
	}
	
	public void setOperationsBank(OperationsBank operationsBank){
		this.operationsBank = operationsBank;
	}
	
	public void setReward(Reward reward){
		this.reward = reward;
	}
	
	public void setBestPath(Path bestPath){
		this.bestPath = bestPath;
	}
	
	public void apply(SelectPolicy selectPolicy,SimPolicy simPolicy, Parameter param){
		        
		Simulate simulator = new Simulate();
		Select select = new Select();
		Repeat repeat1 = new Repeat();
		Repeat repeat2 = new Repeat();
		Step step = new Step();
		LookAhead lookAhead = new LookAhead();

		simulator.setSimPolicy(simPolicy);//this parameter is only for Simulate search component
		simulator.setOperationsBank(operationsBank);
		simulator.setRoot(root);
		simulator.setMaxDepth(maxDepth);
		simulator.setReward(reward);
		simulator.setBestPath(bestPath);/*THIS bestPath IS SHARED AMONG ALL THE SEARCH COMPONENTS AND Parameter class*/
		/*NOT NECESSARY*/simulator.setSearchPolicy(this);//why need to set search policy here? Havent we set the simpolicy on top already?
		simulator.setParam(param);//param here is all the parameters of the program
		
		repeat1.copyParameters(simulator);//parameters here mean all the SearchPolicy, BestPath, OperationBank, Root, Depth, Reward, and parameters of the program
		repeat1.setNbRepetitions(param.nbRepetitions1);
		repeat1.setSearchComponent(simulator);//SET simulator as the search component inside the repeat1
		
		lookAhead.copyParameters(simulator);
		lookAhead.setSearchComponent(repeat1);//SET repeat1 as the search component inside the lookAhead
		
		select.copyParameters(simulator);
		select.setSelectPolicy(selectPolicy);
		select.setSearchComponent(lookAhead);//SET lookAhead as the search component inside the select
		
		repeat2.copyParameters(simulator);
		repeat2.setNbRepetitions(param.nbRepetitions2);
		repeat2.setSearchComponent(select);//SET select as the search component inside the repeat2
		
		step.copyParameters(simulator);
		step.setSearchComponent(repeat2);//SET repeat2 as the search component inside the step
		
		//After the above we will get the algorithm: 
		//Step(Repeat(nbRepetitions2,Select(LookAhead(Repeat(nbRepetitions1,Simulate)))))
		
		step.apply(this.sequence, this.root);		
	}

	public void setDepth(int depth) {
		this.maxDepth = depth;		
	}
	
}
