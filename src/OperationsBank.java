import java.util.LinkedList;

abstract class OperationsBank {
	
	public LinkedList<Operation> operations;
	
	public Operation getOperation(int opNum){
		for(Operation operation : this.operations){
			if(operation.num == opNum){
				return operation;
			}
		}
		System.out.println("Operation not found");
		return null;
	}
	
	public int getSize(){
		return this.operations.size();
	}
	
	abstract void addOperation(Operation op);
	
	abstract void display();
}
