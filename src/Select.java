import java.util.LinkedList;

/* Select search component:
 * Stores statistics on the outcomes of sub-searches
 * Uses these information to bias sub-searches selection
 * Has 3 steps: select, sub-search and back-propagation
 */
public class Select extends SearchComponent{

	public SelectPolicy selectPolicy;
	public SearchComponent searchComp;

	public Select(){
		super();
		setSearchComponent(null);
		setSelectPolicy(null);
		this.name = "Select";
	}

	public void setSearchComponent(SearchComponent searchComp){
		this.searchComp = searchComp;
	}

	public void setSelectPolicy(SelectPolicy selectPolicy){
		this.selectPolicy = selectPolicy;
	}

	public SearchComponent getSearchComponent(){
		return this.searchComp;
	}

	public SelectPolicy getSelectPolicy(){
		return this.selectPolicy;
	}

	@SuppressWarnings("unchecked")
	public void apply(LinkedList<Operation> sequence, Node state){
		//System.out.println("Select!");
		
		int t = state.level;

		Operation op;
		Node nextState;
		LinkedList<Operation> nextSeq = new LinkedList<Operation>();
		LinkedList<Operation> seq = new LinkedList<Operation>();
		Node prevState;
		Node node;

		nextSeq = (LinkedList<Operation>) sequence.clone();

		nextState = state;	

		//if the selection hits the stop filter, it will evaluate the sequence (yield) and stop
		//The STOP FILTER is for the purpose of accepting any sequence of filters having size < maxDepth
		if((nextSeq.size() != 0) && (nextSeq.getLast().name.equals("Stop"))){
			//invoke(this.searchComp, nextSeq, nextState);
			yield(nextSeq);
			return;
		}

		/*----------SELECTION: STEP DOWN THE TREE UNTIL REACHING A LEAF NODE, THEN EXPAND-----------*/
		for(int i = t; i < maxDepth; i++){

			this.param.setSelect(i);//only for displaying the progress and report

			//if the selection reaches the leaf node of the current selection tree
			if(nextState.hasChildren() == false){
				//add/expand all the possible children to newNode
				nextState.expand();
			}
			
			// Select the next operation/action using UCB-1
			op = this.selectPolicy.apply(nextState);
			nextSeq.add(op);
			nextState = nextState.OperationFindChild(op);//get the next state given the current state and an operation

			//if node i (nextState) is never visited then node (i-1) is a leaf node, and node (i-1) has just been expanded
			if(nextState.nbStateSelect == 0){
				break;
			}
		}

		seq = (LinkedList<Operation>) nextSeq.clone();

		node = nextState.copy();
		/*--- SUB-SEARCH ---*/
		invoke(this.searchComp, seq, node); 
		prevState = nextState;

		/*------ BACK PROPAGATION -----*/
		while(prevState.parent != null){
			op = prevState.prevOperation;
			prevState.nbStateSelect++;
			prevState = prevState.parent;
			prevState.nbActionSelect[op.num - 1] = prevState.nbActionSelect[op.num - 1] + 1;
			prevState.rewardActionSelect[op.num - 1] = prevState.rewardActionSelect[op.num - 1] + this.bestPath.bestReward;
		}
		prevState.nbStateSelect++;
	}
}
