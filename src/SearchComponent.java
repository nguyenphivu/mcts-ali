import java.util.LinkedList;

/* super class to search components
 * 
 */
public class SearchComponent{

	public Path bestPath;//each search component stores bestPath and bestReward (embedded in bestPath) locally
	public OperationsBank operationsBank;
	public Reward reward;
	public Node root;
	public SearchPolicy searchPolicy;
	public int maxDepth;
	public boolean stop;
	public Parameter param;
	public String name;

	public SearchComponent(){
		//setOperationsBank(null);
		setReward(null);
		setRoot(null);
		setSearchPolicy(null);
		this.stop = false;
		//setDepth();
		param = null;
	}

	public void setBestPath(Path path){
		this.bestPath = path;
	}

	public void setOperationsBank(OperationsBank operationsBank){
		this.operationsBank = operationsBank;
	}

	public void setReward(Reward reward){
		this.reward = reward;
	}

	public void setRoot(Node root){
		this.root = root;
	}

	public void setSearchPolicy(SearchPolicy searchPolicy){
		this.searchPolicy = searchPolicy;
	}

	public void setMaxDepth(int depth){
		this.maxDepth = depth;
	}

	public void setParam(Parameter param){
		this.param = param;
	}

	public Path getBestPath(){
		return this.bestPath;
	}

	public Reward getReward(){
		return this.reward;
	}

	public Node getRoot(){
		return this.root;
	}

	public OperationsBank getOperationsBank(){
		return this.operationsBank;
	}

	public SearchPolicy getSearchPolicy(){
		return this.searchPolicy;
	}

	public int getDepth(){
		return this.maxDepth;
	}

	/* Copy parameters from a search component to another
	 * 
	 */
	public void copyParameters(SearchComponent searchComponent){
		this.setBestPath(searchComponent.getBestPath());
		this.setRoot(searchComponent.getRoot());
		this.setMaxDepth(searchComponent.maxDepth);
		this.setOperationsBank(searchComponent.getOperationsBank());
		this.setReward(searchComponent.getReward());
		this.setSearchPolicy(searchComponent.searchPolicy);
		this.setParam(searchComponent.param);
	}

	/* Yield method
	 * Calculates the reward for a final sequence. If it is better the best one
	 * stored so fare, the best reward becomes this one and the corresponding 
	 * sequence becomes the best one.
	 * Each call of this method takes one unit of budget. If maximum budget is reached
	 * the computation is stopped and the best result is the one stored so far.
	 */
	@SuppressWarnings("unchecked")
	public void yield(LinkedList<Operation> sequence){
		//System.out.println("Yield!");
		double r = 0.0;
		LinkedList<LinkedList<Double>> rewards = new LinkedList<LinkedList<Double>>();
		
		//ACTUALLY run filters and evaluate the result
		rewards = reward.calculate(sequence, root);//rewards[0]: the worst reward among all the rewards of the training images
												   //rewards[1]: list of all the rewards of training images
		r = rewards.getFirst().getFirst();//get the worst reward among all the rewards of the training images
		
		//evaluate the result and update the bestPath
		if(r > this.bestPath.bestReward){
			this.bestPath.bestReward = r;
			this.bestPath.setBestBranch((LinkedList<Operation>) sequence.clone());
			this.bestPath.setBestRewards((LinkedList<Double>) rewards.getLast().clone());//PV: Why getLast?
		}

		searchPolicy.numCalls = searchPolicy.numCalls + 1;
		param.setBudgetUsed(searchPolicy.numCalls);

		if(searchPolicy.numCalls == searchPolicy.budget){
			System.out.println((char)27 + "[31mOut of budget!");
			param.setOutOfBudget();
			this.stop();	
		}	
	}

	public void stop() {	
		this.stop = true;
	}
	
	/* If the maximum depth is reached, it calls the yield method
	 * Otherwise, the sub-search component is called.
	 */
	public void invoke(SearchComponent searchComp, LinkedList<Operation> sequence, Node state ){
		//System.out.println("Invoke!");
		int t = state.level;

		if(t < maxDepth){
			searchComp.apply(sequence, state);
		}else{
			yield(sequence);//update the bestPath
		}
	}

	public void apply(LinkedList<Operation> sequence, Node state){

	}
}
