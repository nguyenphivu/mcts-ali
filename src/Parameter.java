import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Date;
import java.util.LinkedList;

/* This class is used to store all the information related
 * to the MCTS program: parameters, system features and computation details.
 * It allows extracting parameters from a .txt file ( int, double, String,
 * char and boolean types).
 * Edit reports (txt report or template for a pdf report) including all these information.
 */
abstract class Parameter {
	File param;
	public String stringVal;
	public char charVal;
	public int intVal;
	public double doubleVal;
	public boolean booleanVal;

	int depth;
	int budget;
	int nbRepetitions1;
	int nbRepetitions2;
	double hyperParam;
	long time;
	int h, m, s, d;
	Date start;
	Date end;
	int step;
	int repeat1;
	int repeat2;
	int lookahead;
	int select;
	int simulate;
	boolean outOfBudget;

	String selectPolicy;
	String simPolicy;
	double ram;
	String processor;
	String os;
	double javaVersion;

	Path bestPath;

	boolean reportEdited;
	boolean runComplited;
	boolean computationCompleted;

	String reportAdr;
	String mctsTemplate;

	int seed;
	int budgetUsed;

	LinkedList<Double> visitedNodesFraction;
	public PrintWriter fractionOutputWriter;
	
	
	public void setUpFractionGraphDataFile(String filePath) {
		try {
			fractionOutputWriter = new PrintWriter(new FileWriter(filePath+"/fraction_data.dat"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void finishFractionGraphDataWriting(){
		this.fractionOutputWriter.close();
	}
	
	public void addVisitedNodesFraction(double fraction){
		this.visitedNodesFraction.add(fraction);
	}
	
	public void setMctsTemplate(String template){
		this.mctsTemplate = template;
	}
	
	public void setBudgetUsed(int budget){
		this.budgetUsed = budget;
	}

	public void setSeed(int seed){
		this.seed = seed;
	}

	public void setOutOfBudget(){
		this.outOfBudget = true;
	}

	public void setOs(String os){
		this.os = os;
	}

	public void setJavaVersion(double version){
		this.javaVersion = version;
	}

	public void setRam(double ram){
		this.ram = ram;
	}

	public void setProcessor(String proc){
		this.processor = proc;
	}

	public void setReportAdr(String adr){
		this.reportAdr = adr;
	}

	public void setComputationCompleted(){
		this.computationCompleted = true;
	}

	public void computeResult(){

	}

	public void setSelectPolicy(String policy){
		this.selectPolicy = policy;
	}

	public void setSimPolicy(String policy){
		this.simPolicy = policy;
	}

	public void setRunCompleted(){
		this.runComplited = true;
	}

	public void setBestPath(Path path){
		this.bestPath = path;
	}

	public void setDepth(double e){
		this.depth = (int) e;
	}

	public void setBudget(double e){
		this.budget = (int) e;
	}

	public void setNbRepetitions1(double e){
		this.nbRepetitions1 = (int) e;
	}

	public void setNbRepetitions2(double e){
		this.nbRepetitions2 = (int) e;
	}

	public void setHyperParam(double param){
		this.hyperParam = param;
	}

	public void setStep(int step){
		this.step = step;
	}

	public void setRepeat1(int repeat1){
		this.repeat1 = repeat1;
	}

	public void setRepeat2(int repeat2){
		this.repeat2 = repeat2;
	}

	public void setLookahead(int lookahead){
		this.lookahead = lookahead;
	}

	public void setSelect(int select){
		this.select = select;
	}

	public void setSimulate(int simulate){
		this.simulate = simulate;
	}

	public void getStartTime(){
		start = new Date();
	}

	public void getEndTime(){
		end = new Date();
	}
/* Calculate the run time in days, hours, minutes and seconds
 * 
 */
	public void setRunTime(){

		time = end.getTime()-start.getTime();		
		d = (int) (time/(24*60*60*1000));
		h = (int) ((time/(60*60*1000))%24);
		m = (int) ((time/(60*1000))%60);
		s = (int) ((time/(1000))%60);
	}
	/* Calculates an approximative progression percentage and displays it.
	 * 
	 */
	abstract void displayProgress();

	/* Edit a txt report
	 * 
	 */
	abstract void editReport();
	
/* Edit a LateX template that can be used to build a pdf report.
 * 
 */
	abstract void editLateXReport();

	public void setParamFile(String path){
		this.param = new File(path);
	}
/* Get a parameter from a txt file given its name and the separator used
 * if the name is found, checks if the parameter's name is valid or not
 * The parameter's value is then extracted in String type and parsed to
 * the appropriate type.
 * Some special characters are allowed for a parameter of type String
 * 
 */
	public void getParam(String name, char separator, String type){
		String line = "";
		String res = "";

		InputStream ips;
		try {
			ips = new FileInputStream(this.param);
			InputStreamReader ipsr = new InputStreamReader(ips);
			BufferedReader out = new BufferedReader(ipsr);
			try {
				while ((line = out.readLine()) != null){
					if(line.contains(name)){
						if(this.isValid(name, line, separator)){
							int index = line.lastIndexOf(separator)+1;
							while(((Character)line.charAt(index)).compareTo(' ') == 0){
								index++;
							}
							for(int j = index; j < line.length(); j++ ){
								if((Character.getNumericValue(line.charAt(j)) < 36) && (Character.getNumericValue(line.charAt(j)) >= 0) ){
									res = res+line.charAt(j);
								}else if(((Character)line.charAt(j)).compareTo('.') == 0){
									res = res + line.charAt(j);
								}else if(((Character)line.charAt(j)).compareTo('/') == 0){
									res = res + line.charAt(j);
								}else if(((Character)line.charAt(j)).compareTo('_') == 0){
									res = res + line.charAt(j);
								}else if(((Character)line.charAt(j)).compareTo('-') == 0){
									res = res + line.charAt(j);
								}else if(((Character)line.charAt(j)).compareTo('@') == 0){
									res = res + line.charAt(j);
								}else if(((Character)line.charAt(j)).compareTo(')') == 0){
									res = res + line.charAt(j);
								}else if(((Character)line.charAt(j)).compareTo('(') == 0){
									res = res + line.charAt(j);
								}else if(((Character)line.charAt(j)).compareTo(' ') == 0){
									break;
								}else{
									res = "";
									break;
								}
							}
							break;
						}else{
							continue;
						}
					}
				}
				if(res.equals("")){
					System.out.println((char)27 + "[31mParameter '"+name+"' not valid!");
					System.out.print((char)27 + "[37m");
					return;
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();

		}
		try{
			if(type.equals("string")){
				this.stringVal = res;
			}else if(type.equals("char")){
				this.charVal = res.charAt(0);
			}else if(type.equals("double")){
				this.doubleVal = Double.parseDouble(res);
			}else if(type.equals("int")){
				this.intVal = Integer.parseInt(res);
			}else if(type.equals("boolean")){
				if(res.equals("true")){
					this.booleanVal = true;
				}else{
					this.booleanVal = false;
				}
			}
		}catch (NumberFormatException e){
			System.out.println((char)27 + "[31mParameter '"+name+"' not valid!");
			e.printStackTrace();
			System.out.print((char)27 + "[37m");
		}
	}

	public String getString(String name, char separator){
		getParam(name, separator, "string");
		return this.stringVal;
	}

	public char getChar(String name, char separator){
		getParam(name, separator, "char");
		return this.charVal;
	}

	public int getInt(String name, char separator){
		getParam(name, separator, "int");
		return this.intVal;
	}

	public double getDouble(String name, char separator){
		getParam(name, separator, "double");
		return this.doubleVal;
	}

	public boolean getBoolean(String name, char separator){
		getParam(name, separator, "boolean");
		return this.booleanVal;
	}

	public boolean isValid(String name, String line, char separator){

		Character c;

		int firstInd = line.indexOf(name);
		int lastInd = firstInd + name.length();
		int sepInd;
		if(line.indexOf(separator) == -1){
			sepInd = line.length();
		}else if(line.indexOf(separator) < firstInd){
			sepInd = line.length();
		}else{
			sepInd = line.indexOf(separator);
		}

		for(int i = 0; i > firstInd; i++){
			c = line.charAt(i);
			if(c.compareTo(' ')!=0){
				return false;
			}
		}

		for(int i = lastInd; i < sepInd; i++){
			c = line.charAt(i);
			if(c.compareTo(' ')!=0){
				return false;
			}
		}
		return true;
	}
}
