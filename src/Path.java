import java.util.LinkedList;

/* Path that leads to the final state
 * Includes: root node, branch(operations sequence) and reward.
 */
public class Path {

	private Node root;
	public LinkedList<Operation> bestBranch;//list of filters (best sequence of actions)
	public LinkedList<Double> bestRewards;//the best rewards of all the learning set images 
	public double bestReward; //the worst reward among the bestRewards
	public int maxDepth; //max depth of the tree

	public Path() {
		setDepth(0);
		this.bestBranch = new LinkedList<Operation>();
		this.bestRewards = new LinkedList<Double>();
		setBestReward(0);
	}

	public void setRoot(Node root){
		this.root = root.copy();
	}

	@SuppressWarnings("unchecked")
	public void setBestBranch(LinkedList<Operation> bestBranch){
		this.bestBranch.clear();
		this.bestBranch = (LinkedList<Operation>) bestBranch.clone();
	}

	public void setBestRewards(LinkedList<Double> rewards){
		this.bestRewards = rewards;
	}
	
	public void setBestReward(double r){
		this.bestReward = r;
	}

	public void setDepth(int depth){
		this.maxDepth = depth;
	}

	public Node getRoot(){
		return this.root;
	}

	public LinkedList<Operation> getBestBranch(){
		return this.bestBranch;
	}

	public double getBestReward(){
		return this.bestReward;
	}

	public int getDepth(){
		return this.maxDepth;
	}
	
	public void display() {
		Operation op;
		System.out.println("Best reward : " + this.bestReward);
		System.out.print("sequence : ");
		for(int i = 0; i < this.bestBranch.size(); i++){
			op = this.bestBranch.get(i);
			System.out.print("Operation " + op.num + " : ");
			System.out.print(op.name + ", ");
		}
	}
}
