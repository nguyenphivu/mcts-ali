/* Defines the simulation policy to use
 * 
 */
abstract class SimPolicy /*Simulation policy*/ {

	abstract Operation apply(Node nextState);

}
