import java.util.LinkedList;

import org.opencv.core.Mat;

/*Node class for image processing problem
 * extends Node abstract class
 * All the initial images are in grey level
 */

@SuppressWarnings("unused")
public class ImgProcNode extends Node{

	public String state;

	public ImgProcNode(){

		setParent(null);
		this.childrenList = new LinkedList<Node>();
		this.childrenEdges = new LinkedList<Edge>();
		this.nbActionSelect = new int[0];
		this.rewardActionSelect = new double[0];
		this.nbStateSelect = 0;
		setLevel(0);
		this.prevOperation = new ImgProcOperation();
		this.state = "Grey";
		this.operationsBank = new ImgProcOperationsBank();
		this.depthMax = 0;
	}

	@SuppressWarnings("unchecked")
	public Node copy(){

		ImgProcNode newNode = new ImgProcNode();

		if(this.childrenList != null){
			for(int j = 0; j < this.childrenList.size(); j++){
				newNode.childrenList.add(this.childrenList.get(j));
			}			
		}
		newNode.setParent(this.parent);
		newNode.childrenEdges.clear();
		newNode.childrenEdges = (LinkedList<Edge>) this.childrenEdges.clone();
		newNode.nbActionSelect = this.nbActionSelect.clone();
		newNode.rewardActionSelect = this.rewardActionSelect.clone();
		newNode.nbStateSelect = this.nbStateSelect;
		newNode.setLevel(this.level);
		newNode.prevOperation = this.prevOperation;
		newNode.state = this.state;
		newNode.setOperationsBank(this.operationsBank);
		newNode.depthMax = this.depthMax;

		return (Node)newNode;
	}

	/*add a child node
	 * (non-Javadoc)
	 * @see Node#addChild(Node, Operation)
	 * Check the filter to apply. If it's a thresholding,
	 * the state changes to binary
	 */
	public void addChild(Node node, Operation operation){
		ImgProcNode child = new ImgProcNode();
		child = (ImgProcNode) node;
		child.parent = this;
		child.level = this.level + 1;
		child.prevOperation = (ImgProcOperation) operation;
		//Set the state of the child
		if(operation.name.equals("Thresholding")){
			child.state = "Bin";
		}else{
			child.state = this.state;
		}
		child.setOperationsBank(this.operationsBank);
		child.depthMax = this.depthMax;
		this.childrenList.add(child);//add to the children list
		addChildEdge((Node)child, operation);
	}

	//EVERY TIME THE EXPAND IS CALLED: EXPAND ALL THE POSSIBLE CHILDREN
	/* Expends the current node according to the possible filters:
	 * - If the node's state is grey level, possible filters are
	 * Gabor, Gaussian and thresholding. If the max depth is reached
	 * and the node's state still grey level, the only filters possible
	 * are thresholding since we need to end with a binary state.
	 * - If the node's state is binary, only binary filters (dilation, erosion,
	 * openning and closing)
	 * - In all the cases (either "grey" or "binary"), a child corresponding to the stop filter is created.
	 * (non-Javadoc)
	 * @see Node#expend()
	 */
	public void expand(){

		if(this.childrenList == null){
			this.childrenList = new LinkedList<Node>();
		}else{
			this.childrenList.clear();//this's for safety, because if we call expand for a node, it's not supposed to have children
		}

		ImgProcOperation op = new ImgProcOperation();

		if(this.state.equals("Grey")){
			//If the max depth is reached
			//and the node's state still grey level, the only filters possible
			//are thresholding since we need to end with a binary state.
			if(this.level == this.depthMax-1){
				int grey = ((ImgProcOperationsBank)this.operationsBank).nbGreyScale;
				//expand all the POSSIBLE children
				for(int j = 1; j < ((ImgProcOperationsBank)this.operationsBank).nbThreshold+1; j++){
					ImgProcOperation operation = new ImgProcOperation();
					operation = (ImgProcOperation) this.operationsBank.getOperation(grey+j);//
					ImgProcNode child = new ImgProcNode();
					child.parent = new ImgProcNode();

					//if(operation.num != this.prevOperation.num){
					this.addChild((Node)child, (Operation)operation);
					//}
				}
			}
			//If the node's state is grey level, possible filters are
			// Gabor, Gaussian and thresholding.
			else{
				int greyAndThreshold = ((ImgProcOperationsBank)this.operationsBank).nbGreyScale + ((ImgProcOperationsBank)this.operationsBank).nbThreshold;
				//expand all the POSSIBLE children
				for (int i = 0; i < greyAndThreshold; i++) {
					ImgProcNode child = new ImgProcNode();
					child.parent = new ImgProcNode();

					op = (ImgProcOperation)this.operationsBank.operations.get(i);
					//if(op.num != this.prevOperation.num){
					this.addChild((Node)child, (Operation)op);
					//}
				}
			}
		}
		//If the node's state is binary, only binary filters (dilation, erosion,
		//openning and closing)
		else if(this.state.equals("Bin")){
			int grey = ((ImgProcOperationsBank)this.operationsBank).nbGreyScale + ((ImgProcOperationsBank)this.operationsBank).nbThreshold;
			int binary = grey + ((ImgProcOperationsBank)this.operationsBank).nbBinary;
			for (int i = grey+1; i < binary+1; i++) {
				ImgProcNode child = new ImgProcNode();
				child.parent = new ImgProcNode();

				op = (ImgProcOperation)this.operationsBank.operations.get(i);
				//if(op.num != this.prevOperation.num){
				this.addChild((Node)child, (Operation)op);
				//}
			}
		}else{
			System.out.println("Expend not possible");
		}
		
		//In all the cases (either "grey" or "binary"), a child corresponding to the stop filter is created.
		ImgProcNode child = new ImgProcNode();
		child.parent = new ImgProcNode();
		op = (ImgProcOperation)this.operationsBank.operations.getLast();//getLast means get the stop filter
		this.addChild((Node)child, (Operation)op);
	}

	
	//public boolean hasChildren() {
	//	return super.hasChildren();
	//}

	public void display(){
		System.out.println("Level = " + this.level);
		System.out.println("Operation = number "+Integer.toString(this.prevOperation.num)+" "+this.prevOperation.name);
		System.out.println("Nb visits = " + this.nbStateSelect);
		for(int i = 0; i < this.nbActionSelect.length; i++){
			System.out.println("Op "+i+" sel "+this.nbActionSelect[i]+" tms "+" rew "+this.rewardActionSelect[i]);
		}
	}

	public void displayFamily(){
		this.display();
		if(this.parent!=null){
			System.out.println("Move to parent!");
			((ImgProcNode) this.parent).displayFamily();
		}
	}
}