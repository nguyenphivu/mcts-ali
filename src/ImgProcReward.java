import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import mcib3d.utils.ThreadUtil;

import org.opencv.core.*;
import org.opencv.highgui.Highgui;

/* Calculates the jaccard score for all the laerning set images
 * and returns the worst one.
 * Multithreading is used to improve the calculation time.
 */
public class ImgProcReward extends Reward {

	public LinkedList<Mat> input/* the list original images */;
	public LinkedList<Mat> labeledImages;
	public LinkedList<Double> finalRewardList/* for all the labelled images */;
	LinkedList<Integer> seq /* a sequence of filter numbers */;
	public static ArrayList<ArrayList<ConnectedComponent>> labeledConCompLists = new ArrayList<ArrayList<ConnectedComponent>>();
	//public double alpha;
	public double radius;
	public PrintWriter outputWriter;
	public static ImgProcParameters param;

	public ImgProcReward() {

		this.output = new LinkedList<Operation>();
		this.labeledImages = new LinkedList<Mat>();
		this.input = new LinkedList<Mat>();
		this.finalRewardList = new LinkedList<Double>();
		this.seq = new LinkedList<Integer>();
		
	}

	public void setParam(ImgProcParameters p){
		this.param= p;
	}
	
	public void setUpGraphDataFile(String filePath) {
		try {
			outputWriter = new PrintWriter(new FileWriter(filePath+"/reward_data.dat"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void finishGraphDataWriting(){
		this.outputWriter.close();
	}
	
	// MULTI-THREADING: Run a sequence of filters for all the original images,
	// and then calculate rewards
	public LinkedList<LinkedList<Double>> calculate(
			LinkedList<Operation> filterSeq, Node root) {

		//output[0]: the worst reward among all the rewards of the training images
		//output[1]: list of all the rewards of training images
		LinkedList<LinkedList<Double>> output = new LinkedList<LinkedList<Double>>();//output only has 2 elements
		LinkedList<Double> worstOutput = new LinkedList<Double>();//this list has only 1 element
		double worstFinalReward = 1.0;
		this.finalRewardList.clear();
		for (int k = 0; k < this.input.size(); k++) {
			this.finalRewardList.add(0.0);
		}
		this.seq.clear();

		// LinkedList<Integer> seq = new LinkedList<Integer>();
		for (int i = 0; i < filterSeq.size(); i++) {
			seq.add(filterSeq.get(i).num);
		}

		final AtomicInteger ai/* a static counter SHARED among ALL threads */= new AtomicInteger(
				0);

		// Multi-threading: create as many thread as #CPUs/cores of the machine
		Thread[] threads = ThreadUtil
				.createThreadArray(ThreadUtil.getNbCpus()/*
														 * get the number of
														 * CPUs/cores of the
														 * machine
														 */);

		for (int ithread = 0; ithread < threads.length; ithread++) {
			threads[ithread] = new Thread() {
				// 1 thread will work on 1 original image
				@Override
				public void run() {
					Mat labeledImg = new Mat();
					Mat inputImg = new Mat();
					Mat outputImg = new Mat();
					double jaccard = 0.0;
					double matchingScore = 0.0;
					double finalScore = 0.0;

					// k starts from 0 to # original images, and shared among
					// all the threads
					for (int k = ai.getAndIncrement(); k < input.size(); k = ai
							.getAndIncrement()) {

						input.get(k).copyTo(inputImg);
						labeledImages.get(k).copyTo(labeledImg);

						int success = FiltersBank.runFilters(seq, inputImg,
								outputImg);// run the list of filters
						if (success == 0)
							System.out.println("Running the array of filters failed at one of the filters.");
						// else
						// System.out.println("Running the array of filters successful.");

						jaccard = jaccardScore(labeledImg, outputImg);
						matchingScore = matchingScore(outputImg,
								labeledConCompLists.get(k),ImgProcReward.this.labeledImages.get(k),new Mat(), new Mat());
						finalScore = param.alpha * jaccard + (1 - param.alpha)* matchingScore;
						
						finalRewardList.set(k, finalScore);
					}
				}
			};
		}

		// START executing all the threads and waiting until ALL the threads
		// finish
		ThreadUtil.startAndJoin(threads);

		// Get the worst reward
		for (int i = 0; i < finalRewardList.size(); i++) {
			if (finalRewardList.get(i) < worstFinalReward) {
				worstFinalReward = finalRewardList.get(i);
			}
		}

		//write the worstFinalReward to data.dat for graph plotting
		try{
			param.rewardCounter++;
			this.outputWriter.println(param.rewardCounter + "\t" + worstFinalReward);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		worstOutput.add(worstFinalReward);
		output.add(worstOutput);
		output.add(finalRewardList);

		// Get the Java runtime
		Runtime runtime = Runtime.getRuntime();
		// Run the garbage collector
		runtime.gc();

		return output;

	}

	/*// Final_reward= Alpha*Jaccard_reward + (1-Alpha)*Matching_Reward
	public void setAlpha(double a) {
		this.alpha = a;
	}*/

	// input: 2 binary images with the same size
	// output: the jaccard score of the two images (|intersection of black
	// pixels|/|union of black pixels|)
	public static double jaccardScore(Mat img1, Mat img2) {
		double jaccard = 0;

		// the 2 input images should have the same size
		if (img1.width() == img2.width() && img1.height() == img2.height()) {
			int intersection = 0;
			int union = 0;
			for (int i = 0; i < img1.height(); i++) {
				for (int j = 0; j < img1.width(); j++) {
					double[] data1 = img1.get(i, j);
					double[] data2 = img2.get(i, j);
					if (data1[0] < 50 && data2[0] < 50) { // if the 2 pixels at
															// the same position
															// in the two images
															// are both black
						intersection++;
						union++;
					} else if (data1[0] < 50 && data2[0] >= 50)
						union++;
					else if (data2[0] < 50 && data1[0] >= 50)
						union++;
				}
			}
			jaccard = (double) intersection / (double) union;
		} else
			return -1;// failed to calculate Jaccard score
		return jaccard;
	}

	// srcImg MUST be a BINARY image
	public static void findConnectedComponents(Mat srcImg, Mat destImg,
			ArrayList<ConnectedComponent> connectedComponentList /*, int minArea*/ ) {
		int minArea = (int)ImgProcReward.param.minArea;//500
		Hashtable<Integer, ConnectedComponent> conCompHt = new Hashtable<Integer, ConnectedComponent>();// from label to connected component
		int[][] labelMatrix = new int[srcImg.height()][srcImg.width()]; // all elements will be initialized with 0
		int issuedLabel = 0;

		// System.out.println("Start finding connected components...");

		int temp = 0;
		for (int i = 1; i < srcImg.height() - 1; i++) {
			for (int j = 1; j < srcImg.width() - 1; j++) {
				double[] data = srcImg.get(i, j);
				if (data[0] <= 50) { // black pixel
					// if(data[0] > 50){
					if (labelMatrix[i][j - 1] != 0) {// the left neighbour pixel has label (and of course is black pixel)
						labelMatrix[i][j] = labelMatrix[i][j - 1]; // label pixel (i,j), and admit it to the left neighbor pixel's connected component
						
						// UPDATE THE CONNECTED COMPONENT
						ConnectedComponent conComp = conCompHt
								.get(labelMatrix[i][j - 1]);
						if (conComp == null) {
							System.out.println(i + "\t" + j + "\t"
									+ labelMatrix[i][j]);
							System.out.println(labelMatrix[i][j - 1]);
						}
						conComp.noPixels++;
						if (conComp.right < j)
							conComp.right = j;// no need to update the left, top and bottom boundaries of the connected component
						conCompHt.put(labelMatrix[i][j - 1], conComp);

						// MERGING THE CONNECTED COMPONENT WITH ITS NEIGHBOUR
						// CONNECTED COMPONENTS (If existing any)
						ConnectedComponent neighbourConComp;
						if (labelMatrix[i - 1][j - 1] != 0
								&& labelMatrix[i - 1][j - 1] != labelMatrix[i][j - 1]) {
							temp = labelMatrix[i - 1][j - 1];
							for (int e = j - 1; e < srcImg.width(); e++) {// update the labels of all pixels at the same line
								// and have the same label as pixel (i-1,j-1)
								if (labelMatrix[i - 1][e] == temp)
									labelMatrix[i - 1][e] = labelMatrix[i][j - 1];
							}
							for (int e = j - 2; e > 0; e--) {// update the labels of all pixels at the same line as
								// pixel (i,j) and have the same label as pixel
								// (i-1,j-1)
								if (labelMatrix[i][e] == temp)
									labelMatrix[i][e] = labelMatrix[i][j - 1];
							}

							neighbourConComp = conCompHt.get(temp);
							conComp.noPixels += neighbourConComp.noPixels;
							if (neighbourConComp.left < conComp.left)
								conComp.left = neighbourConComp.left;
							if (neighbourConComp.right > conComp.right)
								conComp.right = neighbourConComp.right;
							if (neighbourConComp.top < conComp.top)
								conComp.top = neighbourConComp.top;// no need to update the bottom boundary
							conCompHt.put(labelMatrix[i][j - 1], conComp);
							conCompHt.remove(temp);// delete the neighbour conComp after it is merged
						}
						if (labelMatrix[i - 1][j] != 0
								&& labelMatrix[i - 1][j] != labelMatrix[i][j - 1]) {
							temp = labelMatrix[i - 1][j];
							for (int e = j; e < srcImg.width(); e++) {// update the labels of all pixelsat the same line and have the same label as pixel (i-1,j)
								if (labelMatrix[i - 1][e] == temp)
									labelMatrix[i - 1][e] = labelMatrix[i][j - 1];
							}
							for (int e = j - 2; e > 0; e--) {// update the labels of all pixels at the same line as pixel (i,j) and have the same label as pixel (i-1,j)
								if (labelMatrix[i][e] == temp)
									labelMatrix[i][e] = labelMatrix[i][j - 1];
							}

							neighbourConComp = conCompHt.get(temp);
							conComp.noPixels += neighbourConComp.noPixels;
							if (neighbourConComp.left < conComp.left)
								conComp.left = neighbourConComp.left;
							if (neighbourConComp.right > conComp.right)
								conComp.right = neighbourConComp.right;
							if (neighbourConComp.top < conComp.top)
								conComp.top = neighbourConComp.top;// no need to update the bottom boundary
							conCompHt.put(labelMatrix[i][j - 1], conComp);

							conCompHt.remove(temp);// delete the neighbour conComp after it is merged
						}
						// Log.d("tag1", i + " " + j);
						if (labelMatrix[i - 1][j + 1] != 0
								&& labelMatrix[i - 1][j + 1] != labelMatrix[i][j - 1]) {
							temp = labelMatrix[i - 1][j + 1];
							for (int e = j + 1; e < srcImg.width(); e++) {// update the labels of all pixels at the same line and have the same label as pixel (i-1,j+1)
								if (labelMatrix[i - 1][e] == temp)
									labelMatrix[i - 1][e] = labelMatrix[i][j - 1];
							}
							for (int e = j - 2; e > 0; e--) {// update the labels of all pixels at the same line as pixel (i,j) and have the same label as pixel (i-1,j+1)
								if (labelMatrix[i][e] == temp)
									labelMatrix[i][e] = labelMatrix[i][j - 1];
							}

							neighbourConComp = conCompHt.get(temp);
							conComp.noPixels += neighbourConComp.noPixels;
							if (neighbourConComp.left < conComp.left)
								conComp.left = neighbourConComp.left;
							if (neighbourConComp.right > conComp.right)
								conComp.right = neighbourConComp.right;
							if (neighbourConComp.top < conComp.top)
								conComp.top = neighbourConComp.top;// no need to update the bottom boundary
							conCompHt.put(labelMatrix[i][j - 1], conComp);
							conCompHt.remove(temp);// delete the neighbour conComp after it is merged
						}
					} else if (labelMatrix[i - 1][j - 1] != 0) {// the top-left neighbour pixel has label (and of course is black pixel)
						labelMatrix[i][j] = labelMatrix[i - 1][j - 1]; // label pixel (i,j), and admit it to the top-left neighbor pixel's connected component
						// UPDATE THE CONNECTED COMPONENT
						ConnectedComponent conComp = conCompHt
								.get(labelMatrix[i - 1][j - 1]);
						conComp.noPixels++;
						if (conComp.right < j)
							conComp.right = j;
						if (conComp.bottom < i)
							conComp.bottom = i;// no need to update the left and top boundaries of the connected component
						conCompHt.put(labelMatrix[i - 1][j - 1], conComp);

						// MERGING THE CONNECTED COMPONENT WITH ITS NEIGHBOUR
						// CONNECTED COMPONENTS (If existing any)
						ConnectedComponent neighbourConComp;
						if (labelMatrix[i - 1][j] != 0
								&& labelMatrix[i - 1][j] != labelMatrix[i - 1][j - 1]) {
							// This will never happen because labelMatrix[i-1][j] must become equal to labelMatrix[i-1][j-1] when line (i-1) was scanned previously
						}
						if (labelMatrix[i - 1][j + 1] != 0
								&& labelMatrix[i - 1][j + 1] != labelMatrix[i - 1][j - 1]) {
							temp = labelMatrix[i - 1][j + 1];
							for (int e = j + 1; e < srcImg.width(); e++) {// update the labels of all pixels at the same line and have the same label as pixel (i-1,j+1)
								if (labelMatrix[i - 1][e] == temp)
									labelMatrix[i - 1][e] = labelMatrix[i - 1][j - 1];
							}
							for (int e = j - 2; e > 0; e--) {// update the labels of all pixels at the same line as pixel (i,j) and have the same label as pixel (i-1,j+1)
								if (labelMatrix[i][e] == temp)
									labelMatrix[i][e] = labelMatrix[i - 1][j - 1];
							}

							neighbourConComp = conCompHt.get(temp);
							conComp.noPixels += neighbourConComp.noPixels;
							if (neighbourConComp.left < conComp.left)
								conComp.left = neighbourConComp.left;
							if (neighbourConComp.right > conComp.right)
								conComp.right = neighbourConComp.right;
							if (neighbourConComp.top < conComp.top)
								conComp.top = neighbourConComp.top;//no need to update the bottom boundary
							conCompHt.put(labelMatrix[i][j - 1], conComp);
							conCompHt.remove(temp);// delete the neighbour conComp after it is merged
						}

					} else if (labelMatrix[i - 1][j] != 0) {// the top neighbour pixel has label (and of course is black pixel)
						labelMatrix[i][j] = labelMatrix[i - 1][j]; // label pixel (i,j), and admit it to the top neighbour pixel's connected component
						// UPDATE THE CONNECTED COMPONENT
						ConnectedComponent conComp = conCompHt
								.get(labelMatrix[i - 1][j]);
						conComp.noPixels++;
						if (conComp.bottom < i)
							conComp.bottom = i;// no need to update the left, right and top boundaries of the connected component
						conCompHt.put(labelMatrix[i - 1][j], conComp);

						// MERGING THE CONNECTED COMPONENT WITH ITS NEIGHBOUR CONNECTED COMPONENTS (If existing any) ConnectedComponent neighbourConComp;
						if (labelMatrix[i - 1][j + 1] != 0
								&& labelMatrix[i - 1][j + 1] != labelMatrix[i - 1][j]) {
							// This will never happen because labelMatrix[i-1][j+1] must become equal to labelMatrix[i-1][j] when line (i-1) was scanned previously
						}
					} else if (labelMatrix[i - 1][j + 1] != 0) {// the top-right neighbour pixel has label (and of course is black pixel)
						labelMatrix[i][j] = labelMatrix[i - 1][j + 1]; // label pixel (i,j), and admit it to the top-right neighbour pixel's connected component
						// UPDATE THE CONNECTED COMPONENT
						ConnectedComponent conComp = conCompHt
								.get(labelMatrix[i - 1][j + 1]);
						conComp.noPixels++;
						if (conComp.bottom < i)
							conComp.bottom = i;// no need to update the left, right and top boundaries of the connected component
						if (conComp.left > j)
							conComp.left = j;
						conCompHt.put(labelMatrix[i - 1][j + 1], conComp);
					} else {// there is none black pixels among the 4 neighbour pixels of pixel (i,j)
							// CREATE A NEW CONNECTED COMPONENT FOR pixel (i,j)
						issuedLabel++;
						labelMatrix[i][j] = issuedLabel;
						ConnectedComponent conComp = new ConnectedComponent();
						conComp.noPixels = 1;
						conComp.label = issuedLabel;
						conComp.left = conComp.right = j;
						conComp.top = conComp.bottom = i;
						conCompHt.put(labelMatrix[i][j], conComp);
					}
				}// end if (data[0]<=50){ //pixel (i,j) is black
			}// end for j
		}// end for i
			// System.out.println("Finnished finding connected components.");

		// LISTING ALL THE CONNECTED COMPONENTS
		Enumeration<Integer> en = conCompHt.keys();
		int label = 0;
		while (en.hasMoreElements()) {
			label = en.nextElement();
			// ONLY TAKE CONNECTED COMPONENTS WITH noPixels >= minArea
			if (conCompHt.get(label).noPixels >= minArea)
				connectedComponentList.add(conCompHt.get(label));
			else{
				ConnectedComponent cc= conCompHt.get(label);
				ArrayList<Point> arr= new ArrayList<Point>();
				arr.add(new Point(cc.left, cc.top));
				arr.add(new Point(cc.right, cc.top));
				arr.add(new Point(cc.right, cc.bottom));
				arr.add(new Point(cc.left, cc.bottom));
				
				MatOfPoint mop= new MatOfPoint();
				mop.fromList(arr);
				Core.fillConvexPoly(destImg, mop, new Scalar(255,255,255));
			}
		}

		//Mat tempImg= new Mat(srcImg.size(), CvType.CV_8UC3);
		//Rect rectCrop= new Rect();
		//Mat roi= new Mat();
		// DRAW ALL THE CONNECTED COMPONENTS TO destImg (using rectangle bounds)
		for (int i = 0; i < connectedComponentList.size(); i++) {
			//if (connectedComponentList.get(i).noPixels >= minArea){
				//Display only connected components with area>= minArea, all the rest are erased
				/*rectCrop = new Rect(connectedComponentList.get(i).left, connectedComponentList.get(i).top,
						connectedComponentList.get(i).right-connectedComponentList.get(i).left, 
						connectedComponentList.get(i).bottom-connectedComponentList.get(i).top);
				roi = srcImg.submat(rectCrop);
				roi.copyTo(new Mat(tempImg, rectCrop));*/
				Core.rectangle(destImg, new Point(
						connectedComponentList.get(i).left,
						connectedComponentList.get(i).top), new Point(
						connectedComponentList.get(i).right,
						connectedComponentList.get(i).bottom), new Scalar(0, 0,
						255), 2);
			//}
		}
		//tempImg.copyTo(destImg);
		// System.out.println("Finnished drawing connected components.");
	}

	public static double matchingScore(Mat computedImg,
			ArrayList<ConnectedComponent> labeledFragments, Mat labeledImg, Mat outputComputedImg, Mat outputLabeledImg) {
		double matchingScore = 0;
		Mat outputImg = new Mat(computedImg.size(), CvType.CV_8UC3);
		computedImg.copyTo(outputImg);
		ArrayList<ConnectedComponent> conCompList = new ArrayList<ConnectedComponent>();

		ImgProcReward.findConnectedComponents(computedImg, outputImg,
				conCompList);
		outputImg.copyTo(outputComputedImg);
		int radius = (int)ImgProcReward.param.radius;//70;// put this as parameter later
		ArrayList<Map.Entry<Integer, Integer>> matchingList = stableMatching(
				conCompList, labeledFragments, radius);

		//Draw all the matchings on the two output images
		//outputComputedImg= computedImg.clone();
		//outputLabeledImg= labeledImg.clone();
		int[] computedImgMatchCheckArray= new int[conCompList.size()];//default init values are 0
		int[] labeledImgMatchCheckArray= new int[labeledFragments.size()];//default init values are 0
		
		for (int i = 0; i < matchingList.size(); i++) {
			int computedFragmentIndex= matchingList.get(i).getKey();
			int labeledFragmentIndex= matchingList.get(i).getValue();
			Core.rectangle(outputComputedImg, new Point(
					conCompList.get(computedFragmentIndex).left,
					conCompList.get(computedFragmentIndex).top), new Point(
					conCompList.get(computedFragmentIndex).right,
					conCompList.get(computedFragmentIndex).bottom), new Scalar(255, 0,
						0), 5);
			Core.putText(outputComputedImg, i+"", new Point(
					conCompList.get(computedFragmentIndex).left,
					conCompList.get(computedFragmentIndex).top), Core.FONT_HERSHEY_COMPLEX, 2, new Scalar(255, 0,
							0));
			Core.rectangle(outputLabeledImg, new Point(
					labeledFragments.get(labeledFragmentIndex).left,
					labeledFragments.get(labeledFragmentIndex).top), new Point(
					labeledFragments.get(labeledFragmentIndex).right,
					labeledFragments.get(labeledFragmentIndex).bottom), new Scalar(255, 0,
						0), 5);
			Core.putText(outputLabeledImg, i+"", new Point(
					labeledFragments.get(labeledFragmentIndex).left,
					labeledFragments.get(labeledFragmentIndex).top), Core.FONT_HERSHEY_COMPLEX, 2, new Scalar(255, 0,
							0));
			computedImgMatchCheckArray[computedFragmentIndex]= 1;
			labeledImgMatchCheckArray[labeledFragmentIndex]= 1;
		}
		
		//Draw all the un-matched fragments on the two output images
		for (int i=0;i<computedImgMatchCheckArray.length;i++){
			if (computedImgMatchCheckArray[i]==0){
				Core.rectangle(outputComputedImg, new Point(
						conCompList.get(i).left,
						conCompList.get(i).top), new Point(
						conCompList.get(i).right,
						conCompList.get(i).bottom), new Scalar(0, 0,
							255), 2);
			}
		}
		for (int i=0;i<labeledImgMatchCheckArray.length;i++){
			if (labeledImgMatchCheckArray[i]==0){
				Core.rectangle(outputLabeledImg, new Point(
						labeledFragments.get(i).left,
						labeledFragments.get(i).top), new Point(
						labeledFragments.get(i).right,
						labeledFragments.get(i).bottom), new Scalar(0, 0,
							255), 2);
			}
		}
		
		//matchingScore = (double) matchingList.size()
		//		/ (double) labeledFragments.size();//= coverage rate (recall)
		
		matchingScore = (double) matchingList.size()
				/ (double) (labeledFragments.size()+conCompList.size()-matchingList.size());
		
		return matchingScore;
	}

	
	public static ArrayList<Map.Entry<Integer/* man */, Integer/* woman */>> stableMatching(
			ArrayList<ConnectedComponent> list1/* men */,
			ArrayList<ConnectedComponent> list2/* women */, double radius) {
		ArrayList<Map.Entry<Integer/* man */, Integer/* woman */>> matchingList = new ArrayList<Map.Entry<Integer/* man */, Integer/* woman */>>();
		double[/* men */][/* women */] dis = computeDistanceMatrix(list1,
				list2);

		/*--Compute the array of men preferences--*/
		// Each man has an arrayList of women in decreasing order of his
		// preferences that he not yet proposed THAT HE NOT YET PROPOSED.
		ArrayList<ArrayList<Map.Entry<Integer, Double>>> menPreferences = new ArrayList<ArrayList<Map.Entry<Integer, Double>>>();
		for (int i = 0; i < list1.size(); i++) {
			Hashtable<Integer, Double> ht = new Hashtable<Integer, Double>();// from
																				// women
																				// indices
																				// to
																				// distance
			for (int j = 0; j < list2.size(); j++) {
				ht.put(j, dis[i][j]);
			}
			ArrayList<Map.Entry<Integer, Double>> sortedList = sortByValues(ht);
			menPreferences.add(sortedList);
		}

		ArrayList<Integer> currentFiancesOfWomen = new ArrayList<Integer>();// element=
																			// -1
																			// if
																			// the
																			// woman
																			// is
																			// free
		for (int i = 0; i < list2.size(); i++)
			currentFiancesOfWomen.add(-1);// all the women are free at the
											// beginning

		ArrayList<Integer> freeMenAndCanProposeList = new ArrayList<Integer>();
		for (int i = 0; i < list1.size(); i++)
			freeMenAndCanProposeList.add(i);// all the men can propose at the
											// beginning

		/*----PROPOSE-AND-REJECT ALGORITHM----*/
		while (freeMenAndCanProposeList.size() > 0) {
			int selectedMan = freeMenAndCanProposeList.get(0);
			int hisCurrentBestPreferredWoman = menPreferences.get(selectedMan)
					.get(0).getKey();
			// the selectedMan starts proposing to hisCurrentBestPreferredWoman
			menPreferences.get(selectedMan).remove(0);// hisCurrentBestPreferredWoman
														// is marked as proposed
														// in his preference
														// list

			if (currentFiancesOfWomen.get(hisCurrentBestPreferredWoman) == -1/*
																			 * the
																			 * selected
																			 * man
																			 * 's
																			 * current
																			 * best
																			 * preferred
																			 * woman
																			 * is
																			 * currently
																			 * free
																			 */) {
				currentFiancesOfWomen.set(hisCurrentBestPreferredWoman,
						selectedMan);// assign hisCurrentBestPreferredWoman to
										// him (the selected man)
				freeMenAndCanProposeList.remove(0); // the selected man is no
													// longer free
			} else {// the woman is already engaged
				int her = hisCurrentBestPreferredWoman;
				int herCurrentFiance = currentFiancesOfWomen.get(her);

				if (dis[herCurrentFiance][her] > dis[selectedMan][her]) {// she
																			// prefers
																			// the
																			// selectedMan
																			// to
																			// herCurrentFiance
					if (menPreferences.get(herCurrentFiance).size() > 0)// if
																		// herCurrentFiance
																		// can
																		// still
																		// propose
																		// (to
																		// another
																		// woman)
						freeMenAndCanProposeList.add(herCurrentFiance);
					currentFiancesOfWomen.set(her, selectedMan);// assign her to
																// the selected
																// man
					freeMenAndCanProposeList.remove(0); // the selected man is
														// no longer free
				} else {// hisCurrentBestPreferredWoman rejects him (the
						// selectedMan)
					if (menPreferences.get(selectedMan).size() == 0)
						freeMenAndCanProposeList.remove(0); // there's no more
															// woman for the
															// selected man to
															// propose
				}

			}
		}

		// Finally, remove all the remained free women, keep only the
		// engaged/matched ones
		// And also remove all the matches having distance > radius
		for (int i = 0; i < currentFiancesOfWomen.size(); i++) {
			if (currentFiancesOfWomen.get(i) != -1) {
				// System.out.println(currentFiancesOfWomen.get(i) + "  i= " + i
				// + " size=" + currentFiancesOfWomen.size());
				if (dis[currentFiancesOfWomen.get(i)][i] <= radius) {
					Map.Entry<Integer, Integer> match = new AbstractMap.SimpleEntry<Integer, Integer>(
							currentFiancesOfWomen.get(i), i);
					matchingList.add(match);
				}
			}
		}

		return matchingList;
	}

	public static double[][] computeDistanceMatrix(
			ArrayList<ConnectedComponent> list1,
			ArrayList<ConnectedComponent> list2) {
		double[][] dis = new double[list1.size()][list2.size()];

		for (int i = 0; i < list1.size(); i++)
			for (int j = 0; j < list2.size(); j++) {
				double x1 = (double) (list1.get(i).left + list1.get(i).right) / 2.0;
				double y1 = (double) (list1.get(i).top + list1.get(i).bottom) / 2.0;
				double x2 = (double) (list2.get(j).left + list2.get(j).right) / 2.0;
				double y2 = (double) (list2.get(j).top + list2.get(j).bottom) / 2.0;
				dis[i][j] = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2)
						* (y1 - y2));
			}

		return dis;
	}

	// Sorting a hashtable by values
	public static ArrayList<Map.Entry<Integer, Double>> sortByValues(
			Hashtable<Integer, Double> t) {
		// Transfer as List and sort it
		ArrayList<Map.Entry<Integer, Double>> l = new ArrayList<Map.Entry<Integer, Double>>(
				t.entrySet());
		Collections.sort(l, new Comparator<Map.Entry<Integer, Double>>() {

			public int compare(Map.Entry<Integer, Double> o1,
					Map.Entry<Integer, Double> o2) {
				// return o2.getValue().compareTo(o1.getValue());//descending
				// order
				return o1.getValue().compareTo(o2.getValue());// ascending order
			}
		});
		return l;
	}

}
